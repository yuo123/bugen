package com.picherski.bugen.activities;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.picherski.bugen.R;
import widgets.BuGenWidget;

import java.io.*;

/**
 * The configuration screen for the {@link BuGenWidget BuGenWidget} AppWidget.
 */
public class BuGenButtonConfigureActivity extends Activity {

    private static final String PREFS_NAME = "widgets.BuGenWidget";
    private static final String NAME_PREF_PREFIX_KEY = "widget_name_";
    private static final String PATH_PREF_PREFIX_KEY = "widget_path_";
    private static final String LOG_TAG = "BuGenButtonConfigureActivity";
    private View addWidgetBtn;
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    Uri fileUri = null;
    TextView mAppWidgetText;
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = BuGenButtonConfigureActivity.this;
            try {
                // When the button is clicked, store the string locally
                String widgetText = mAppWidgetText.getText().toString();
                String path = saveFileLocally();
                savePref(context, mAppWidgetId, widgetText, path);

                // It is the responsibility of the configuration activity to update the app widget
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                BuGenWidget.initAppWidget(context, appWidgetManager, mAppWidgetId);

                // Make sure we pass back the original appWidgetId
                Intent resultValue = new Intent();
                resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                setResult(RESULT_OK, resultValue);
                finish();
            } catch (Throwable t) {
                BuGenWidget.LogThrowable(t, context);
            }
        }
    };

    private String saveFileLocally() {
        if (fileUri == null)
            throw new IllegalStateException("saveFileLocally called when fileUri is null");

        try {
            InputStream in = getContentResolver().openInputStream(fileUri);
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_MUSIC), String.valueOf(mAppWidgetId) + ".mp3");
            if (!file.createNewFile()) {
                file.delete();
                file.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(file);
            copyStream(in, out);
            in.close();
            out.close();
            return file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Selected file not found", e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024]; // Adjust if you want
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }


    // Write the prefix to the SharedPreferences object for this widget
    static void savePref(Context context, int appWidgetId, String text, String audioPath) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(NAME_PREF_PREFIX_KEY + appWidgetId, text);
        prefs.putString(PATH_PREF_PREFIX_KEY + appWidgetId, audioPath);
        prefs.apply();
    }

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource
    public static Pair<String, String> loadPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String titleValue = prefs.getString(NAME_PREF_PREFIX_KEY + appWidgetId, null);
        if (titleValue == null)
            titleValue = context.getString(R.string.appwidget_text);
        String path = prefs.getString(PATH_PREF_PREFIX_KEY + appWidgetId, null);
        return new Pair<>(titleValue, path);
    }

    public static void removePref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(NAME_PREF_PREFIX_KEY + appWidgetId);
        prefs.remove(PATH_PREF_PREFIX_KEY + appWidgetId);
        prefs.apply();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.bu_gen_button_configure);
        addWidgetBtn = findViewById(R.id.addWidgetBtn);
        mAppWidgetText = (EditText) findViewById(R.id.nameInput);
        findViewById(R.id.pickFileBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT);
                pickIntent.setType("audio/*");
                startActivityForResult(pickIntent, 1);
            }
        });
        addWidgetBtn.setOnClickListener(mOnClickListener);
        addWidgetBtn.setEnabled(false);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            fileUri = data.getData();
            addWidgetBtn.setEnabled(true);
        }
    }
}

