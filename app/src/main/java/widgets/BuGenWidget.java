package widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.util.Pair;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.picherski.bugen.R;
import com.picherski.bugen.activities.BuGenButtonConfigureActivity;

import java.io.*;
import java.util.Calendar;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link BuGenButtonConfigureActivity BuGenButtonConfigureActivity}
 */
public class BuGenWidget extends AppWidgetProvider implements MediaPlayer.OnCompletionListener {

    private static final String SOUND_BTN_CLICK_ACTION = "BuGenWidget.SOUND_BTN_CLICK_ACTION";
    private static final String PATH_EXTRA = "BuGenWidget.PATH_EXTRA";
    private static MediaPlayer mp;

    public static void initAppWidget(Context context, AppWidgetManager appWidgetManager,
                                     int appWidgetId) {
        Pair<String, String> pref = BuGenButtonConfigureActivity.loadPref(context, appWidgetId);
        if (pref.first != null && pref.second != null) {
            // Construct the RemoteViews object
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.bu_gen_button);
            views.setTextViewText(R.id.nameText, pref.first);
            views.setOnClickPendingIntent(R.id.soundBtn, makePendingIntent(context, appWidgetId, pref.second));

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    private static PendingIntent makePendingIntent(Context context, int id, String path) {
        Intent intent = new Intent(context, BuGenWidget.class);
        intent.setAction(SOUND_BTN_CLICK_ACTION);
        if (path == null) Log.w("BuGenWidget", "Null path");
        intent.putExtra(PATH_EXTRA, path);
        Log.d("BuGenWidget", "Initializing click to play path " + intent.getStringExtra(PATH_EXTRA));
        return PendingIntent.getBroadcast(context, id, intent, 0);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equals(SOUND_BTN_CLICK_ACTION)) {
                String path = intent.getStringExtra(PATH_EXTRA);
                Log.d("BuGenWidget", "Playing sound file: " + path);
                if (path != null) {
                    try {
                        mp = new MediaPlayer();
                        mp.setDataSource(path);
                        mp.prepare();
                        mp.setOnCompletionListener(this);
                        mp.start();
                    } catch (IOException e) {
                        throw new RuntimeException("Error playing button sound", e);
                    }
                }
            } else
                super.onReceive(context, intent);
        } catch (Throwable t) {
            LogThrowable(t, context);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            String path = BuGenButtonConfigureActivity.loadPref(context, appWidgetId).second;
            BuGenButtonConfigureActivity.removePref(context, appWidgetId);
            Log.d("BuGenWidget", "Deleting audio file: " + path);
            new File(path).delete();
        }
    }

    @Override
    public void onEnabled(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] ids = appWidgetManager.getAppWidgetIds(new ComponentName(context, BuGenWidget.class));
        for (int id : ids) {
            Log.d("BuGenWidget", "performing onEnabled initialization for widget id " + id);
            initAppWidget(context, appWidgetManager, id);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.release();
        BuGenWidget.mp = null;
    }

    public static void LogThrowable(Throwable t, Context c) {
        File file = new File(c.getExternalFilesDir(null), "crash_log.txt");
        PrintStream writer = null;
        try {
            file.createNewFile();
            writer = new PrintStream(file);
            writer.append("------BEGIN CRASH REPORT ON ").append(Calendar.getInstance().getTime().toString()).append(" -------------");
            writer.append("Throwable: ").append(t.getMessage());
            t.printStackTrace(writer);
            writer.append("-----------END CRASH REPORT--------------");
            Toast.makeText(c, "BuGenWidget crashed! Crash report saved at " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            throw new RuntimeException("could not write log file", e);
        } finally {
            if (writer != null)
                writer.close();
        }

    }
}

